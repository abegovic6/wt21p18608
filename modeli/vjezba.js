const Sequelize = require("sequelize")

module.exports = function (sequelize, DataTypes) {
    const Vjezba = sequelize.define('Vjezba', {
        index: {
            type: Sequelize.INTEGER
           },
        naziv: {
            type: Sequelize.STRING
        }
   });
   return Vjezba;
}