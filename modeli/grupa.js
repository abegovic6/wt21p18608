const Sequelize = require("sequelize")
var DataTypes = require('sequelize/lib/data-types');
 
module.exports = function (sequelize, DataTypes) {
    const Grupa = sequelize.define('Grupa', {
       naziv: {
        type: Sequelize.STRING,
        allowNull: false,
        primaryKey: true
       } 
   });
   return Grupa;
}