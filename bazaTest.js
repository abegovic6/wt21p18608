const chai = require('chai');
const chaihttp = require('chai-http');
chai.use(chaihttp);
chai.should();
let assert = chai.assert;

const server = require('./index.js');
const Sequelize = require('sequelize');
const sequelize = require('./baza.js');
const zadatak = require('./modeli/zadatak.js');
//const { describe } = require('mocha');

const Grupa = require('./modeli/grupa.js')(sequelize);
const Student = require('./modeli/student.js')(sequelize);

const Vjezba = require('./modeli/vjezba.js')(sequelize);
const Zadatak = require('./modeli/zadatak.js')(sequelize);

var DataTypes = require('sequelize/lib/data-types');

module.exports = server

Grupa.hasMany(Student, {
    foreignKey: {
        name: 'grupa',
        type: DataTypes.STRING
    }
});

Vjezba.hasMany(Zadatak);


let backUpGrupe = [];
let backUpStudenti = [];
let backUpVjezbe = [];
let backUpZadaci = [];

describe('Testovi za spiralu 4: Baza podataka', function () {
    this.timeout(90000);

    this.beforeAll(done => {
        sequelize.sync().then(() => {
            return Grupa.findAll();
        }).then(listaGrupa => {
            listaGrupa.map(grupa => {
                backUpGrupe.push({
                    naziv: grupa.naziv
                });
            });
            return Student.findAll();
        }).then(listaStudenata => {
            listaStudenata.map(student => {
                backUpStudenti.push({
                    id: student.id,
                    ime: student.ime,
                    prezime: student.prezime,
                    index: student.index,
                    grupa: student.grupa
                });
            });
            return Vjezba.findAll();
        }).then(listaVjezbi => {
            listaVjezbi.map(vjezba => {
                backUpVjezbe.push({
                    id: vjezba.id,
                    index: vjezba.id,
                    naziv: vjezba.id
                });
            });
            return Zadatak.findAll();
        }).then(listaZadataka => {
            listaZadataka.map(zadatak => {
                backUpZadaci.push({
                    id: zadatak.id,
                    naziv: zadatak.naziv,
                    VjezbaId: zadatak.VjezbaId
                });
            });
            console.log("BROJ STUDENATA " + backUpStudenti.length)
            sequelize.sync({ force:true }).then(() => { done(); });
        });
    });

    this.afterEach((done) => {
        sequelize.sync( { force:true } ).then(() => { done(); });
    });

    this.afterAll((done) => {
        Vjezba.bulkCreate(backUpVjezbe).then(() => {
            return Zadatak.bulkCreate(backUpZadaci);
        }).then(() => {
            return Grupa.bulkCreate(backUpGrupe);
        }).then(() => {
            return Student.bulkCreate(backUpStudenti);
        }).then(() => {
            done();
        });
    });


    describe('Testovi: GET/vjezbe', () => {
        it('Test 1: Get uredu', async () => {
            const vjezbe = { brojVjezbi: 4, brojZadataka: [1,2,7,1] };

            //Ubacimo vjezbe u POST
            await chai.request(server).post('/vjezbe').set('Content-Type', 'application/json').send(JSON.stringify(vjezbe));

            const odgovorIzBaze = await chai.request(server).get('/vjezbe').set('Content-Type', 'application/json').send();
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).brojVjezbi, 4, 'Broj vjezbi uredu');
            assert.equal(JSON.parse(odgovorIzBaze.text).brojZadataka.length, 4, 'Broj zadataka uredu');
        });

        it('Test 2: Get uredu', async () => {
            const vjezbe = { brojVjezbi: 6, brojZadataka: [1,2,7,1,9,0] };

            //Ubacimo vjezbe u bazu
            await chai.request(server).post('/vjezbe').set('Content-Type', 'application/json').send(JSON.stringify(vjezbe));

            // provjerimo get
            const odgovorIzBaze = await chai.request(server).get('/vjezbe').set('Content-Type', 'application/json').send();
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).brojVjezbi, 6, 'Broj vjezbi uredu');
            assert.equal(JSON.parse(odgovorIzBaze.text).brojZadataka.length, 6, 'Broj zadataka uredu');
        });
    });

    describe('Testovi: POST/vjezbe', () => {
        it('Test 1: Kreiranje 4 vjezbe - vjezbe uredu', async () => {
            const vjezbe = { brojVjezbi: 4, brojZadataka: [1,2,7,1] };
            let brZadataka = 0;
            vjezbe.brojZadataka.map(vjezba => {
                brZadataka += vjezba;
            });

            //Ubacimo vjezbe u POST
            const odgovorIzBaze = await chai.request(server).post('/vjezbe').set('Content-Type', 'application/json').send(JSON.stringify(vjezbe));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(odgovorIzBaze.text, JSON.stringify(vjezbe), 'Status uredu');

            const vjezbeIzBaze = await Vjezba.findAll();
            assert.equal(vjezbeIzBaze.length, 4, 'U bazi se ne nalazi dobar broj vjezbi.');

            const zadaciIzBaze = await Zadatak.findAll();
            assert.equal(zadaciIzBaze.length, brZadataka, 'U bazi se ne nalazi dobar broj zadataka.')

        });

        it('Test 2: Kreiranje 10 vjezbi - vjezbe uredu', async () => {
            const vjezbe = { brojVjezbi: 10, brojZadataka: [1,2,7,1,0,9,2,3,8,10] };
            let brZadataka = 0;
            vjezbe.brojZadataka.map(vjezba => {
                brZadataka += vjezba;
            });

            //Ubacimo vjezbe u POST
            const odgovorIzBaze = await chai.request(server).post('/vjezbe').set('Content-Type', 'application/json').send(JSON.stringify(vjezbe));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(odgovorIzBaze.text, JSON.stringify(vjezbe), 'Status uredu');

            const vjezbeIzBaze = await Vjezba.findAll();
            assert.equal(vjezbeIzBaze.length, 10, 'U bazi se ne nalazi dobar broj vjezbi.');

            const zadaciIzBaze = await Zadatak.findAll();
            assert.equal(zadaciIzBaze.length, brZadataka, 'U bazi se ne nalazi dobar broj zadataka.')

        });

        it('Test 3: Neispravni podaci', async () => {
            const vjezbe = { "brojVjezbi": 17, "brojZadataka": [1,2,3,4] };
            let brZadataka = 0;
            vjezbe.brojZadataka.map(vjezba => {
                brZadataka += vjezba;
            });

            const greska = { status: "error", data: "Pogrešan parametar brojVjezbi,brojZadataka" };
            //Ubacimo vjezbe u POST
            const odgovorIzBaze = await chai.request(server).post('/vjezbe').set('Content-Type', 'application/json').send(JSON.stringify(vjezbe));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(odgovorIzBaze.text, JSON.stringify(greska), 'Status uredu');

            const vjezbeIzBaze = await Vjezba.findAll();
            assert.equal(vjezbeIzBaze.length, 0, 'U bazi se ne nalazi dobar broj vjezbi.');

            const zadaciIzBaze = await Zadatak.findAll();
            assert.equal(zadaciIzBaze.length, 0, 'U bazi se ne nalazi dobar broj zadataka.')

        });

        it('Test 3: Neispravni podaci ne mijenjaju bazu', async () => {
            let vjezbe = { brojVjezbi: 10, brojZadataka: [1,2,7,1,0,9,2,3,8,10] };
            let brZadataka = 0;
            vjezbe.brojZadataka.map(vjezba => {
                brZadataka += vjezba;
            });

            let odgovorIzBaze = await chai.request(server).post('/vjezbe').set('Content-Type', 'application/json').send(JSON.stringify(vjezbe));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(odgovorIzBaze.text, JSON.stringify(vjezbe), 'Status uredu');

            let vjezbeIzBaze = await Vjezba.findAll();
            assert.equal(vjezbeIzBaze.length, 10, 'U bazi se ne nalazi dobar broj vjezbi.');

            let zadaciIzBaze = await Zadatak.findAll();
            assert.equal(zadaciIzBaze.length, brZadataka, 'U bazi se ne nalazi dobar broj zadataka.')

            vjezbe = { "brojVjezbi": 17, "brojZadataka": [1,2,3,4] };

            let greska = { status: "error", data: "Pogrešan parametar brojVjezbi,brojZadataka" };
            //Ubacimo vjezbe u POST
            odgovorIzBaze = await chai.request(server).post('/vjezbe').set('Content-Type', 'application/json').send(JSON.stringify(vjezbe));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(odgovorIzBaze.text, JSON.stringify(greska), 'Status uredu');

            vjezbeIzBaze = await Vjezba.findAll();
            assert.equal(vjezbeIzBaze.length, 10, 'U bazi se ne nalazi dobar broj vjezbi.');

            zadaciIzBaze = await Zadatak.findAll();
            assert.equal(zadaciIzBaze.length, brZadataka, 'U bazi se ne nalazi dobar broj zadataka.')

        });
    });
    

    describe('Tesotvi: POST/student', () => {
        it('Test 1: Dodavanje prvog studenta', async () => {
            const student = { 
                ime:"Amila", 
                prezime:"Begovic", 
                index:"18608", 
                grupa:"Grupa 1" 
            };

            // Da li je tekst statusa uredu?
            const odgovorIzBaze = await chai.request(server).post('/student').set('Content-Type', 'application/json').send(JSON.stringify(student));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Kreiran student!', 'Student nije kreiran');


            // Da li je uradjen upis u bazu?
            // Da li je kreirana grupa "Grupa 1"?
            const grupe = await Grupa.findAll();
            assert.equal(grupe.length, 1, 'U bazi se ne nalazi dobar broj grupa.');
            assert.equal(grupe[0].naziv, student.grupa, 'Nije dobar naziv grupe');

            // Da li je kreiran dobar student?
            const studenti = await Student.findAll();
            assert.equal(studenti.length, 1, 'U bazi se ne nalazi dobar broj studenata.');
            assert.equal(studenti[0].ime, student.ime, 'Nije dobro ime studenta');
            assert.equal(studenti[0].prezime, student.prezime, 'Nije dobro prezime studenta');
            assert.equal(studenti[0].index, student.index, 'Nije dobar index studenta');
            assert.equal(studenti[0].grupa, student.grupa, 'Nije dobra grupa studenta');
        });

        it('Test 2: Dodavanje dva studenta, različiti indexi, ista grupa', async () => {
            const student1 = { 
                ime:"Amila", 
                prezime:"Begovic", 
                index:"18608-1", 
                grupa:"Grupa 1" 
            };
            const student2 = { 
                ime:"Amila", 
                prezime:"Begovic", 
                index:"18608-2", 
                grupa:"Grupa 1" 
            };

            // Da li je tekst statusa uredu?
            let odgovorIzBaze = await chai.request(server).post('/student').set('Content-Type', 'application/json').send(JSON.stringify(student1));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Kreiran student!', 'Student nije kreiran');
            odgovorIzBaze = await chai.request(server).post('/student').set('Content-Type', 'application/json').send(JSON.stringify(student2));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Kreiran student!', 'Student nije kreiran');


            // Da li je uradjen upis u bazu?
            // Da li je kreirana grupa "Grupa 1"?
            const grupe = await Grupa.findAll();
            assert.equal(grupe.length, 1, 'U bazi se ne nalazi dobar broj grupa.');
            assert.equal(grupe[0].naziv, student1.grupa, 'Nije dobar naziv grupe');

            // Da li su kreirani dobri studenti?
            const studenti = await Student.findAll();
            assert.equal(studenti.length, 2, 'U bazi se ne nalazi dobar broj studenata.');
            assert.equal(studenti[0].ime, student1.ime, 'Nije dobro ime studenta');
            assert.equal(studenti[0].prezime, student1.prezime, 'Nije dobro prezime studenta');
            assert.equal(studenti[0].index, student1.index, 'Nije dobar index studenta');
            assert.equal(studenti[0].grupa, student1.grupa, 'Nije dobra grupa studenta');
            assert.equal(studenti[1].ime, student2.ime, 'Nije dobro ime studenta');
            assert.equal(studenti[1].prezime, student2.prezime, 'Nije dobro prezime studenta');
            assert.equal(studenti[1].index, student2.index, 'Nije dobar index studenta');
            assert.equal(studenti[1].grupa, student2.grupa, 'Nije dobra grupa studenta');
        });

        it('Test 3: Dodavanje dva studenta, različiti indexi, različita grupa', async () => {
            const student1 = { 
                ime:"Amila", 
                prezime:"Begovic", 
                index:"18608-1", 
                grupa:"Grupa 1" 
            };
            const student2 = { 
                ime:"Amila", 
                prezime:"Begovic", 
                index:"18608-2", 
                grupa:"Grupa 2" 
            };

            // Da li je tekst statusa uredu?
            let odgovorIzBaze = await chai.request(server).post('/student').set('Content-Type', 'application/json').send(JSON.stringify(student1));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Kreiran student!', 'Student nije kreiran');
            odgovorIzBaze = await chai.request(server).post('/student').set('Content-Type', 'application/json').send(JSON.stringify(student2));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Kreiran student!', 'Student nije kreiran');


            // Da li je uradjen upis u bazu?
            // Da li su kreirane grupe "Grupa 1" i "Grupa 2"?
            const grupe = await Grupa.findAll();
            assert.equal(grupe.length, 2, 'U bazi se ne nalazi dobar broj grupa.');
            assert.equal(grupe[0].naziv, student1.grupa, 'Nije dobar naziv grupe');
            assert.equal(grupe[1].naziv, student2.grupa, 'Nije dobar naziv grupe');

            // Da li su kreirani dobri studenti?
            const studenti = await Student.findAll();
            assert.equal(studenti.length, 2, 'U bazi se ne nalazi dobar broj studenata.');
            assert.equal(studenti[0].ime, student1.ime, 'Nije dobro ime studenta');
            assert.equal(studenti[0].prezime, student1.prezime, 'Nije dobro prezime studenta');
            assert.equal(studenti[0].index, student1.index, 'Nije dobar index studenta');
            assert.equal(studenti[0].grupa, student1.grupa, 'Nije dobra grupa studenta');
            assert.equal(studenti[1].ime, student2.ime, 'Nije dobro ime studenta');
            assert.equal(studenti[1].prezime, student2.prezime, 'Nije dobro prezime studenta');
            assert.equal(studenti[1].index, student2.index, 'Nije dobar index studenta');
            assert.equal(studenti[1].grupa, student2.grupa, 'Nije dobra grupa studenta');
        });

        it('Test 4: Dodavanje dva studenta, isti indexi, ista grupa', async () => {
            const student1 = { 
                ime:"Amila", 
                prezime:"Begovic", 
                index:"18608", 
                grupa:"Grupa 1" 
            };
            const student2 = { 
                ime:"Amila", 
                prezime:"Begovic", 
                index:"18608", 
                grupa:"Grupa 1" 
            };

            // Da li je tekst statusa uredu?
            let odgovorIzBaze = await chai.request(server).post('/student').set('Content-Type', 'application/json').send(JSON.stringify(student1));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Kreiran student!', 'Student nije kreiran');
            odgovorIzBaze = await chai.request(server).post('/student').set('Content-Type', 'application/json').send(JSON.stringify(student2));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Student sa indexom 18608 već postoji!', 'Student nije kreiran');


            // Da li je uradjen upis u bazu?
            // Da li je kreirana grupa "Grupa 1" samo jednom?
            const grupe = await Grupa.findAll();
            assert.equal(grupe.length, 1, 'U bazi se ne nalazi dobar broj grupa.');
            assert.equal(grupe[0].naziv, student1.grupa, 'Nije dobar naziv grupe');

            // Da li je kreiran dobar student?
            const studenti = await Student.findAll();
            assert.equal(studenti.length, 1, 'U bazi se ne nalazi dobar broj studenata.');
            assert.equal(studenti[0].ime, student1.ime, 'Nije dobro ime studenta');
            assert.equal(studenti[0].prezime, student1.prezime, 'Nije dobro prezime studenta');
            assert.equal(studenti[0].index, student1.index, 'Nije dobar index studenta');
            assert.equal(studenti[0].grupa, student1.grupa, 'Nije dobra grupa studenta');
        });

        it('Test 5: Dodavanje dva studenta, isti indexi, različita grupa', async () => {
            const student1 = { 
                ime:"Amila", 
                prezime:"Begovic", 
                index:"18608", 
                grupa:"Grupa 1" 
            };
            const student2 = { 
                ime:"Amila", 
                prezime:"Begovic", 
                index:"18608", 
                grupa:"Grupa 2" 
            };

            // Da li je tekst statusa uredu?
            let odgovorIzBaze = await chai.request(server).post('/student').set('Content-Type', 'application/json').send(JSON.stringify(student1));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Kreiran student!', 'Student nije kreiran');
            odgovorIzBaze = await chai.request(server).post('/student').set('Content-Type', 'application/json').send(JSON.stringify(student2));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Student sa indexom 18608 već postoji!', 'Student nije kreiran');


            // Da li je uradjen upis u bazu?
            // Da li je kreirana grupa "Grupa 1" samo jednom?
            const grupe = await Grupa.findAll();
            assert.equal(grupe.length, 1, 'U bazi se ne nalazi dobar broj grupa.');
            assert.equal(grupe[0].naziv, student1.grupa, 'Nije dobar naziv grupe');

            // Da li je kreiran dobar student?
            const studenti = await Student.findAll();
            assert.equal(studenti.length, 1, 'U bazi se ne nalazi dobar broj studenata.');
            assert.equal(studenti[0].ime, student1.ime, 'Nije dobro ime studenta');
            assert.equal(studenti[0].prezime, student1.prezime, 'Nije dobro prezime studenta');
            assert.equal(studenti[0].index, student1.index, 'Nije dobar index studenta');
            assert.equal(studenti[0].grupa, student1.grupa, 'Nije dobra grupa studenta');
        });

        it('Test 6: Dodavanje tri studenta studenta, samo dvije grupe', async () => {
            const student1 = { 
                ime:"Amila", 
                prezime:"Begovic", 
                index:"18608-1", 
                grupa:"Grupa 1" 
            };
            const student2 = { 
                ime:"Amila", 
                prezime:"Begovic", 
                index:"18608-2", 
                grupa:"Grupa 2" 
            };
            const student3 = { 
                ime:"Amila", 
                prezime:"Begovic", 
                index:"18608-3", 
                grupa:"Grupa 2" 
            };

            // Da li je tekst statusa uredu?
            let odgovorIzBaze = await chai.request(server).post('/student').set('Content-Type', 'application/json').send(JSON.stringify(student1));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Kreiran student!', 'Student nije kreiran');
            odgovorIzBaze = await chai.request(server).post('/student').set('Content-Type', 'application/json').send(JSON.stringify(student2));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Kreiran student!', 'Student nije kreiran');
            odgovorIzBaze = await chai.request(server).post('/student').set('Content-Type', 'application/json').send(JSON.stringify(student3));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Kreiran student!', 'Student nije kreiran');


            // Da li je uradjen upis u bazu?
            // Da li je kreirana grupa "Grupa 1" i "Grupa 2" samo jednom?
            const grupe = await Grupa.findAll();
            assert.equal(grupe.length, 2, 'U bazi se ne nalazi dobar broj grupa.');
            assert.equal(grupe[0].naziv, student1.grupa, 'Nije dobar naziv grupe');
            assert.equal(grupe[1].naziv, student2.grupa, 'Nije dobar naziv grupe');

            // Da li je kreiran dobar student?
            const studenti = await Student.findAll();
            assert.equal(studenti.length, 3, 'U bazi se ne nalazi dobar broj studenata.');
            assert.equal(studenti[0].ime, student1.ime, 'Nije dobro ime studenta');
            assert.equal(studenti[0].prezime, student1.prezime, 'Nije dobro prezime studenta');
            assert.equal(studenti[0].index, student1.index, 'Nije dobar index studenta');
            assert.equal(studenti[0].grupa, student1.grupa, 'Nije dobra grupa studenta');
            assert.equal(studenti[1].ime, student2.ime, 'Nije dobro ime studenta');
            assert.equal(studenti[1].prezime, student2.prezime, 'Nije dobro prezime studenta');
            assert.equal(studenti[1].index, student2.index, 'Nije dobar index studenta');
            assert.equal(studenti[1].grupa, student2.grupa, 'Nije dobra grupa studenta');
            assert.equal(studenti[2].ime, student3.ime, 'Nije dobro ime studenta');
            assert.equal(studenti[2].prezime, student3.prezime, 'Nije dobro prezime studenta');
            assert.equal(studenti[2].index, student3.index, 'Nije dobar index studenta');
            assert.equal(studenti[2].grupa, student3.grupa, 'Nije dobra grupa studenta');
        });     
        
        it('Test 7: Dodavanje studenta, prazno ime', async () => {
            const student1 = { 
                prezime:"Begovic", 
                index:"18608-1", 
                grupa:"Grupa 1" 
            };

            // Da li je tekst statusa uredu?
            let odgovorIzBaze = await chai.request(server).post('/student').set('Content-Type', 'application/json').send(JSON.stringify(student1));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Neki podatak je prazan!', 'Student je kreiran');


            // Da li je uradjen upis u bazu?
            // Da li je kreirana grupa "Grupa 1"?
            const grupe = await Grupa.findAll();
            assert.equal(grupe.length, 0, 'U bazi se ne nalazi dobar broj grupa.');

            // Da li je kreiran dobar student?
            const studenti = await Student.findAll();
            assert.equal(studenti.length, 0, 'U bazi se ne nalazi dobar broj studenata.');
        });
        
        it('Test 8: Dodavanje studenta, prazno prezime', async () => {
            const student1 = { 
                ime: "Amila",
                index:"18608-1", 
                grupa:"Grupa 1" 
            };

            // Da li je tekst statusa uredu?
            let odgovorIzBaze = await chai.request(server).post('/student').set('Content-Type', 'application/json').send(JSON.stringify(student1));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Neki podatak je prazan!', 'Student je kreiran');


            // Da li je uradjen upis u bazu?
            // Da li je kreirana grupa "Grupa 1"?
            const grupe = await Grupa.findAll();
            assert.equal(grupe.length, 0, 'U bazi se ne nalazi dobar broj grupa.');

            // Da li je kreiran dobar student?
            const studenti = await Student.findAll();
            assert.equal(studenti.length, 0, 'U bazi se ne nalazi dobar broj studenata.');
        });

        it('Test 9: Dodavanje studenta, prazan index', async () => {
            const student1 = { 
                ime: "Amila",
                prezime: "Begovic",
                grupa:"Grupa 1" 
            };

            // Da li je tekst statusa uredu?
            let odgovorIzBaze = await chai.request(server).post('/student').set('Content-Type', 'application/json').send(JSON.stringify(student1));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Neki podatak je prazan!', 'Student je kreiran');


            // Da li je uradjen upis u bazu?
            // Da li je kreirana grupa "Grupa 1"?
            const grupe = await Grupa.findAll();
            assert.equal(grupe.length, 0, 'U bazi se ne nalazi dobar broj grupa.');

            // Da li je kreiran dobar student?
            const studenti = await Student.findAll();
            assert.equal(studenti.length, 0, 'U bazi se ne nalazi dobar broj studenata.');
        });

        it('Test 10: Dodavanje studenta, prazna grupa', async () => {
            const student1 = { 
                ime: "Amila",
                prezime: "Begovic",
                index:"18608" 
            };

            // Da li je tekst statusa uredu?
            let odgovorIzBaze = await chai.request(server).post('/student').set('Content-Type', 'application/json').send(JSON.stringify(student1));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Neki podatak je prazan!', 'Student je kreiran');


            // Da li je uradjen upis u bazu?
            // Da li je kreirana grupa "Grupa 1"?
            const grupe = await Grupa.findAll();
            assert.equal(grupe.length, 0, 'U bazi se ne nalazi dobar broj grupa.');

            // Da li je kreiran dobar student?
            const studenti = await Student.findAll();
            assert.equal(studenti.length, 0, 'U bazi se ne nalazi dobar broj studenata.');
        });
    });

    describe('Tesotvi: PUT/student/:index', () => {
        it('Test 1: Update kad nema ni jedne kreiranog studenta', async () => {
            const student = { 
                ime:"Amila", 
                prezime:"Begovic", 
                index:"18608", 
                grupa:"Grupa 1" 
            };

            // Prvo kreiramo studenta
            // Da li je tekst statusa uredu?
            let odgovorIzBaze = await chai.request(server).post('/student').set('Content-Type', 'application/json').send(JSON.stringify(student));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Kreiran student!', 'Student nije kreiran');

            // Da li je uradjen upis u bazu?
            // Da li je kreirana grupa "Grupa 1"?
            let grupe = await Grupa.findAll();
            assert.equal(grupe.length, 1, 'U bazi se ne nalazi dobar broj grupa.');
            assert.equal(grupe[0].naziv, student.grupa, 'Nije dobar naziv grupe');

            // Da li je kreiran dobar student?
            let studenti = await Student.findAll();
            assert.equal(studenti.length, 1, 'U bazi se ne nalazi dobar broj studenata.');
            assert.equal(studenti[0].ime, student.ime, 'Nije dobro ime studenta');
            assert.equal(studenti[0].prezime, student.prezime, 'Nije dobro prezime studenta');
            assert.equal(studenti[0].index, student.index, 'Nije dobar index studenta');
            assert.equal(studenti[0].grupa, student.grupa, 'Nije dobra grupa studenta');

            // Sad radimo PUT
            odgovorIzBaze = await chai.request(server).put('/student/18608').set('Content-Type', 'application/json').send(JSON.stringify({grupa: "Grupa 2"}));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Promjenjena grupa studentu 18608', 'Student je ažuriran');

            // Da li je kreirana grupa "Grupa 2"?
            grupe = await Grupa.findAll();
            assert.equal(grupe.length, 2, 'U bazi se ne nalazi dobar broj grupa.');
            assert.equal(grupe[0].naziv, student.grupa, 'Nije dobar naziv grupe');
            assert.equal(grupe[1].naziv, 'Grupa 2', 'Nije dobar naziv grupe');

            // Da li je urađen update
            studenti = await Student.findAll();
            assert.equal(studenti.length, 1, 'U bazi se ne nalazi dobar broj studenata.');
            assert.equal(studenti[0].ime, student.ime, 'Nije dobro ime studenta');
            assert.equal(studenti[0].prezime, student.prezime, 'Nije dobro prezime studenta');
            assert.equal(studenti[0].index, student.index, 'Nije dobar index studenta');
            assert.equal(studenti[0].grupa, 'Grupa 2', 'Nije dobra grupa studenta');
        });


        it('Test 2: Update greska student ne postoji', async () => {
            // Radimo PUT sa praznom bazom
            odgovorIzBaze = await chai.request(server).put('/student/18608').set('Content-Type', 'application/json').send(JSON.stringify({grupa: "Grupa 2"}));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Student sa indexom 18608 ne postoji', 'Student je ažuriran');

            // Baza prazna?
            let grupe = await Grupa.findAll();
            assert.equal(grupe.length, 0, 'U bazi se ne nalazi dobar broj grupa.');

            // Baza prazna?
            let studenti = await Student.findAll();
            assert.equal(studenti.length, 0, 'U bazi se ne nalazi dobar broj studenata.');
        });

        it('Test 3: Update ne posalje se grupa', async () => {
            // Radimo PUT sa praznom bazom
            odgovorIzBaze = await chai.request(server).put('/student/18608').set('Content-Type', 'application/json').send(JSON.stringify({}));
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Neki podatak je prazan!', 'Student je ažuriran');

            // Baza prazna?
            let grupe = await Grupa.findAll();
            assert.equal(grupe.length, 0, 'U bazi se ne nalazi dobar broj grupa.');

            // Baza prazna?
            let studenti = await Student.findAll();
            assert.equal(studenti.length, 0, 'U bazi se ne nalazi dobar broj studenata.');
        });
    });

    describe('Tesotvi: POST/batch/student', () => {
        it('Test 1: Kreiranje tri studenta, različiti indexi, iste grupe', async () => {
            const string = "Amila,Begovic,18608-1,Grupa 1\nAmila,Begovic,18608-2,Grupa 1\nAmila,Begovic,18608-3,Grupa 1";

            // Prvo kreiramo studenta
            // Da li je tekst statusa uredu?
            let odgovorIzBaze = await chai.request(server).post('/batch/student').set('Content-Type', 'text/plain').send(string);
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Dodano 3 studenata!', 'Studenti nisu kreirani');

            // Da li je kreirana grupa "Grupa 1"?
            let grupe = await Grupa.findAll();
            assert.equal(grupe.length, 1, 'U bazi se ne nalazi dobar broj grupa.');
            assert.equal(grupe[0].naziv, "Grupa 1", 'Nije dobar naziv grupe');

            // Da li su kreirani svi studenti?
            let studenti = await Student.findAll({
                order: ['index']
            });
            assert.equal(studenti.length, 3, 'U bazi se ne nalazi dobar broj studenata.');
            assert.equal(studenti[0].ime, "Amila", 'Nije dobro ime studenta');
            assert.equal(studenti[0].prezime, "Begovic", 'Nije dobro prezime studenta');
            assert.equal(studenti[0].index, "18608-1", 'Nije dobar index studenta');
            assert.equal(studenti[0].grupa, "Grupa 1", 'Nije dobra grupa studenta');
            assert.equal(studenti[1].ime, "Amila", 'Nije dobro ime studenta');
            assert.equal(studenti[1].prezime, "Begovic", 'Nije dobro prezime studenta');
            assert.equal(studenti[1].index, "18608-2", 'Nije dobar index studenta');
            assert.equal(studenti[1].grupa, "Grupa 1", 'Nije dobra grupa studenta');
            assert.equal(studenti[2].ime, "Amila", 'Nije dobro ime studenta');
            assert.equal(studenti[2].prezime, "Begovic", 'Nije dobro prezime studenta');
            assert.equal(studenti[2].index, "18608-3", 'Nije dobar index studenta');
            assert.equal(studenti[2].grupa, "Grupa 1", 'Nije dobra grupa studenta');
        });


        it('Test 2: Kreiranje tri studenta, različiti indexi, različite grupe', async () => {
            const string = "Amila,Begovic,18608-1,Grupa 1\nAmila,Begovic,18608-2,Grupa 2\nAmila,Begovic,18608-3,Grupa 3";

            // Prvo kreiramo studenta
            // Da li je tekst statusa uredu?
            let odgovorIzBaze = await chai.request(server).post('/batch/student').set('Content-Type', 'text/plain').send(string);
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Dodano 3 studenata!', 'Studenti nisu kreirani');

            // Da li je kreirana grupa "Grupa 1"?
            let grupe = await Grupa.findAll();
            assert.equal(grupe.length, 3, 'U bazi se ne nalazi dobar broj grupa.');
            assert.equal(grupe[0].naziv, "Grupa 1", 'Nije dobar naziv grupe');
            assert.equal(grupe[1].naziv, "Grupa 2", 'Nije dobar naziv grupe');
            assert.equal(grupe[2].naziv, "Grupa 3", 'Nije dobar naziv grupe');

            // Da li su kreirani svi studenti?
            let studenti = await Student.findAll({
                order: ['index']
            });
            assert.equal(studenti.length, 3, 'U bazi se ne nalazi dobar broj studenata.');
            assert.equal(studenti[0].ime, "Amila", 'Nije dobro ime studenta');
            assert.equal(studenti[0].prezime, "Begovic", 'Nije dobro prezime studenta');
            assert.equal(studenti[0].index, "18608-1", 'Nije dobar index studenta');
            assert.equal(studenti[0].grupa, "Grupa 1", 'Nije dobra grupa studenta');
            assert.equal(studenti[1].ime, "Amila", 'Nije dobro ime studenta');
            assert.equal(studenti[1].prezime, "Begovic", 'Nije dobro prezime studenta');
            assert.equal(studenti[1].index, "18608-2", 'Nije dobar index studenta');
            assert.equal(studenti[1].grupa, "Grupa 2", 'Nije dobra grupa studenta');
            assert.equal(studenti[2].ime, "Amila", 'Nije dobro ime studenta');
            assert.equal(studenti[2].prezime, "Begovic", 'Nije dobro prezime studenta');
            assert.equal(studenti[2].index, "18608-3", 'Nije dobar index studenta');
            assert.equal(studenti[2].grupa, "Grupa 3", 'Nije dobra grupa studenta');
        });

        it('Test 3: Kreiranje tri studenta, različiti indexi, različite grupe', async () => {
            const string = "Amila,Begovic,18608-1,Grupa 1\nAmila,Begovic,18608-2,Grupa 2\nAmila,Begovic,18608-3,Grupa 1";

            // Prvo kreiramo studenta
            // Da li je tekst statusa uredu?
            let odgovorIzBaze = await chai.request(server).post('/batch/student').set('Content-Type', 'text/plain').send(string);
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Dodano 3 studenata!', 'Studenti nisu kreirani');

            // Da li je kreirana grupa "Grupa 1"?
            let grupe = await Grupa.findAll();
            assert.equal(grupe.length, 2, 'U bazi se ne nalazi dobar broj grupa.');
            assert.equal(grupe[0].naziv, "Grupa 1", 'Nije dobar naziv grupe');
            assert.equal(grupe[1].naziv, "Grupa 2", 'Nije dobar naziv grupe');

            // Da li su kreirani svi studenti?
            let studenti = await Student.findAll({
                order: ['index']
            });
            assert.equal(studenti.length, 3, 'U bazi se ne nalazi dobar broj studenata.');
            assert.equal(studenti[0].ime, "Amila", 'Nije dobro ime studenta');
            assert.equal(studenti[0].prezime, "Begovic", 'Nije dobro prezime studenta');
            assert.equal(studenti[0].index, "18608-1", 'Nije dobar index studenta');
            assert.equal(studenti[0].grupa, "Grupa 1", 'Nije dobra grupa studenta');
            assert.equal(studenti[1].ime, "Amila", 'Nije dobro ime studenta');
            assert.equal(studenti[1].prezime, "Begovic", 'Nije dobro prezime studenta');
            assert.equal(studenti[1].index, "18608-2", 'Nije dobar index studenta');
            assert.equal(studenti[1].grupa, "Grupa 2", 'Nije dobra grupa studenta');
            assert.equal(studenti[2].ime, "Amila", 'Nije dobro ime studenta');
            assert.equal(studenti[2].prezime, "Begovic", 'Nije dobro prezime studenta');
            assert.equal(studenti[2].index, "18608-3", 'Nije dobar index studenta');
            assert.equal(studenti[2].grupa, "Grupa 1", 'Nije dobra grupa studenta');
        });

        it('Test 4: Kreiranje tri studenta, različiti indexi, ista grupa', async () => {
            const string = "Amila,Begovic,18608-1,Grupa 1\nAmila,Begovic,18608-2,Grupa 1\nAmila,Begovic,18608-3,Grupa 1";

            // Prvo kreiramo studenta
            // Da li je tekst statusa uredu?
            let odgovorIzBaze = await chai.request(server).post('/batch/student').set('Content-Type', 'text/plain').send(string);
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Dodano 3 studenata!', 'Studenti nisu kreirani');

            // Da li je kreirana grupa "Grupa 1"?
            let grupe = await Grupa.findAll();
            assert.equal(grupe.length, 1, 'U bazi se ne nalazi dobar broj grupa.');
            assert.equal(grupe[0].naziv, "Grupa 1", 'Nije dobar naziv grupe');

            // Da li su kreirani svi studenti?
            let studenti = await Student.findAll({
                order: ['index']
            });
            assert.equal(studenti.length, 3, 'U bazi se ne nalazi dobar broj studenata.');
            assert.equal(studenti[0].ime, "Amila", 'Nije dobro ime studenta');
            assert.equal(studenti[0].prezime, "Begovic", 'Nije dobro prezime studenta');
            assert.equal(studenti[0].index, "18608-1", 'Nije dobar index studenta');
            assert.equal(studenti[0].grupa, "Grupa 1", 'Nije dobra grupa studenta');
            assert.equal(studenti[1].ime, "Amila", 'Nije dobro ime studenta');
            assert.equal(studenti[1].prezime, "Begovic", 'Nije dobro prezime studenta');
            assert.equal(studenti[1].index, "18608-2", 'Nije dobar index studenta');
            assert.equal(studenti[1].grupa, "Grupa 1", 'Nije dobra grupa studenta');
            assert.equal(studenti[2].ime, "Amila", 'Nije dobro ime studenta');
            assert.equal(studenti[2].prezime, "Begovic", 'Nije dobro prezime studenta');
            assert.equal(studenti[2].index, "18608-3", 'Nije dobar index studenta');
            assert.equal(studenti[2].grupa, "Grupa 1", 'Nije dobra grupa studenta');
        });

        it('Test 5: Kreiranje tri studenta, isti indexi, ista grupa', async () => {
            const string = "Amila,Begovic,18608-1,Grupa 1\nAmila,Begovic,18608-1,Grupa 1\nAmila,Begovic,18608-2,Grupa 1";

            // Prvo kreiramo studenta
            // Da li je tekst statusa uredu?
            let odgovorIzBaze = await chai.request(server).post('/batch/student').set('Content-Type', 'text/plain').send(string);
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Dodano 2 studenata, a studenti 18608-1 već postoje!', 'Studenti nisu kreirani');

            // Da li je kreirana grupa "Grupa 1"?
            let grupe = await Grupa.findAll();
            assert.equal(grupe.length, 1, 'U bazi se ne nalazi dobar broj grupa.');
            assert.equal(grupe[0].naziv, "Grupa 1", 'Nije dobar naziv grupe');

            // Da li su kreirani svi studenti?
            let studenti = await Student.findAll({
                order: ['index']
            });
            assert.equal(studenti.length, 2, 'U bazi se ne nalazi dobar broj studenata.');
            assert.equal(studenti[0].ime, "Amila", 'Nije dobro ime studenta');
            assert.equal(studenti[0].prezime, "Begovic", 'Nije dobro prezime studenta');
            assert.equal(studenti[0].index, "18608-1", 'Nije dobar index studenta');
            assert.equal(studenti[0].grupa, "Grupa 1", 'Nije dobra grupa studenta');
            assert.equal(studenti[1].ime, "Amila", 'Nije dobro ime studenta');
            assert.equal(studenti[1].prezime, "Begovic", 'Nije dobro prezime studenta');
            assert.equal(studenti[1].index, "18608-2", 'Nije dobar index studenta');
            assert.equal(studenti[1].grupa, "Grupa 1", 'Nije dobra grupa studenta');
        });

        it('Test 5: Kreiranje tri studenta, isti indexi, različita grupa', async () => {
            const string = "Amila,Begovic,18608-1,Grupa 1\nAmila,Begovic,18608-1,Grupa 2\nAmila,Begovic,18608-2,Grupa 2";

            // Prvo kreiramo studenta
            // Da li je tekst statusa uredu?
            let odgovorIzBaze = await chai.request(server).post('/batch/student').set('Content-Type', 'text/plain').send(string);
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Dodano 2 studenata, a studenti 18608-1 već postoje!', 'Studenti nisu kreirani');

            // Da li je kreirana grupa "Grupa 1" i "Grupa 2"?
            let grupe = await Grupa.findAll();
            assert.equal(grupe.length, 2, 'U bazi se ne nalazi dobar broj grupa.');
            assert.equal(grupe[0].naziv, "Grupa 1", 'Nije dobar naziv grupe');
            assert.equal(grupe[1].naziv, "Grupa 2", 'Nije dobar naziv grupe');

            // Da li su kreirani svi studenti?
            let studenti = await Student.findAll({
                order: ['index']
            });
            assert.equal(studenti.length, 2, 'U bazi se ne nalazi dobar broj studenata.');
            assert.equal(studenti[0].ime, "Amila", 'Nije dobro ime studenta');
            assert.equal(studenti[0].prezime, "Begovic", 'Nije dobro prezime studenta');
            assert.equal(studenti[0].index, "18608-1", 'Nije dobar index studenta');
            assert.equal(studenti[0].grupa, "Grupa 1", 'Nije dobra grupa studenta');
            assert.equal(studenti[1].ime, "Amila", 'Nije dobro ime studenta');
            assert.equal(studenti[1].prezime, "Begovic", 'Nije dobro prezime studenta');
            assert.equal(studenti[1].index, "18608-2", 'Nije dobar index studenta');
            assert.equal(studenti[1].grupa, "Grupa 2", 'Nije dobra grupa studenta');
        });

        it('Test 6: Kreiranje tri studenta, isti indexi, različita grupa', async () => {
            const string = "Amila,Begovic,18608-1,Grupa 1\nAmila,Begovic,18608-1,Grupa 2\nAmila,Begovic,18608-1,Grupa 2";

            // Prvo kreiramo studenta
            // Da li je tekst statusa uredu?
            let odgovorIzBaze = await chai.request(server).post('/batch/student').set('Content-Type', 'text/plain').send(string);
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Dodano 1 studenata, a studenti 18608-1,18608-1 već postoje!', 'Studenti nisu kreirani');

            // Da li je kreirana grupa "Grupa 1" i "Grupa 2"?
            let grupe = await Grupa.findAll();
            assert.equal(grupe.length, 1, 'U bazi se ne nalazi dobar broj grupa.');
            assert.equal(grupe[0].naziv, "Grupa 1", 'Nije dobar naziv grupe');

            // Da li su kreirani svi studenti?
            let studenti = await Student.findAll({
                order: ['index']
            });
            assert.equal(studenti.length, 1, 'U bazi se ne nalazi dobar broj studenata.');
            assert.equal(studenti[0].ime, "Amila", 'Nije dobro ime studenta');
            assert.equal(studenti[0].prezime, "Begovic", 'Nije dobro prezime studenta');
            assert.equal(studenti[0].index, "18608-1", 'Nije dobar index studenta');
            assert.equal(studenti[0].grupa, "Grupa 1", 'Nije dobra grupa studenta');
        });

        it('Test 7: Greska neki podatak fali', async () => {
            const string = "Amila,Begovic,,Grupa 1\nAmila,Begovic,18608-1,Grupa 2\nAmila,Begovic,18608-1,Grupa 2";

            // Prvo kreiramo studenta
            // Da li je tekst statusa uredu?
            let odgovorIzBaze = await chai.request(server).post('/batch/student').set('Content-Type', 'text/plain').send(string);
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Nešto nije uredu sa CSV datotekom. Neki podatak fali.', 'Studenti su kreirani');

            // Da li je kreirana grupa "Grupa 1" i "Grupa 2"?
            let grupe = await Grupa.findAll();
            assert.equal(grupe.length, 0, 'U bazi se ne nalazi dobar broj grupa.');

            // Da li su kreirani svi studenti?
            let studenti = await Student.findAll({
                order: ['index']
            });
            assert.equal(studenti.length, 0, 'U bazi se ne nalazi dobar broj studenata.');
        });

        it('Test 8: Greska neki podatak fali', async () => {
            const string = "Amila,Begovic,18608-1,Grupa 1\nAmila,Begovic,18608-1\nAmila,Begovic,18608-1,Grupa 2";

            // Prvo kreiramo studenta
            // Da li je tekst statusa uredu?
            let odgovorIzBaze = await chai.request(server).post('/batch/student').set('Content-Type', 'text/plain').send(string);
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Nešto nije uredu sa CSV datotekom. Neki podatak fali.', 'Studenti su kreirani');

            // Da li je kreirana grupa "Grupa 1" i "Grupa 2"?
            let grupe = await Grupa.findAll();
            assert.equal(grupe.length, 0, 'U bazi se ne nalazi dobar broj grupa.');

            // Da li su kreirani svi studenti?
            let studenti = await Student.findAll({
                order: ['index']
            });
            assert.equal(studenti.length, 0, 'U bazi se ne nalazi dobar broj studenata.');
        });

        it('Test 9: Greska neki podatak fali', async () => {
            const string = "Amila,Begovic,18608-1,Grupa 1\n,Begovic,18608-1\nAmila,Begovic,18608-1,Grupa 2";

            // Prvo kreiramo studenta
            // Da li je tekst statusa uredu?
            let odgovorIzBaze = await chai.request(server).post('/batch/student').set('Content-Type', 'text/plain').send(string);
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Nešto nije uredu sa CSV datotekom. Neki podatak fali.', 'Studenti su kreirani');

            // Da li je kreirana grupa "Grupa 1" i "Grupa 2"?
            let grupe = await Grupa.findAll();
            assert.equal(grupe.length, 0, 'U bazi se ne nalazi dobar broj grupa.');

            // Da li su kreirani svi studenti?
            let studenti = await Student.findAll({
                order: ['index']
            });
            assert.equal(studenti.length, 0, 'U bazi se ne nalazi dobar broj studenata.');
        });

        it('Test 9: Greska neki podatak fali', async () => {
            const string = "Amila,Begovic,18608-1,Grupa 1\nBegovic,18608-1\nAmila,,18608-1,Grupa 2";

            // Prvo kreiramo studenta
            // Da li je tekst statusa uredu?
            let odgovorIzBaze = await chai.request(server).post('/batch/student').set('Content-Type', 'text/plain').send(string);
            assert.equal(odgovorIzBaze.status, 200, 'Postoji neka greška sa bazom');
            assert.equal(JSON.parse(odgovorIzBaze.text).status, 'Nešto nije uredu sa CSV datotekom. Neki podatak fali.', 'Studenti su kreirani');

            // Da li je kreirana grupa "Grupa 1" i "Grupa 2"?
            let grupe = await Grupa.findAll();
            assert.equal(grupe.length, 0, 'U bazi se ne nalazi dobar broj grupa.');

            // Da li su kreirani svi studenti?
            let studenti = await Student.findAll({
                order: ['index']
            });
            assert.equal(studenti.length, 0, 'U bazi se ne nalazi dobar broj studenata.');
        });
    });
})
