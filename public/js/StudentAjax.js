var StudentAjax = (function () {

    const dodajStudenta = function (student,fnCallback) {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function () {
            if (ajax.readyState == 4 && ajax.responseText['status'] == "Neki podatak je prazan!")
                fnCallback(JSON.parse(ajax.responseText)['status'], null);
            else if (ajax.readyState == 4 && ajax.status == 200) {
                fnCallback(null, JSON.parse(ajax.responseText));
            }
            else if (ajax.readyState == 4 && ajax.status == 404)
                fnCallback("error", null);
        }

        ajax.open("POST", "http://localhost:3000/student", true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify(student));
    }

    const postaviGrupu = function (index,grupa,fnCallback) {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function () {
            if (ajax.readyState == 4 && ajax.responseText['status'] == "Neki podatak je prazan!")
                fnCallback(JSON.parse(ajax.responseText)['status'], null);
            else if (ajax.readyState == 4 && ajax.status == 200) {
                fnCallback(null, JSON.parse(ajax.responseText));
            }
            else if (ajax.readyState == 4 && ajax.status == 404)
                fnCallback("error", null);
        }

        ajax.open("PUT", "http://localhost:3000/student/" + index, true);
        ajax.setRequestHeader("Content-Type", "application/json");
        const grupaJson = {
            grupa: grupa
        }
        ajax.send(JSON.stringify(grupaJson));
    }

    const dodajBatch = function (csvStudenti, fnCallback) {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function () {
            if (ajax.readyState == 4 && ajax.responseText['status'] == "Neki podatak je prazan!")
                fnCallback(JSON.parse(ajax.responseText)['status'], null);
            else if (ajax.readyState == 4 && ajax.status == 200) {
                fnCallback(null, JSON.parse(ajax.responseText));
            }
            else if (ajax.readyState == 4 && ajax.status == 404)
                fnCallback("error", null);
        }

        ajax.open("POST", "http://localhost:3000/batch/student", true);
        ajax.setRequestHeader("Content-Type", "text/plain");
        ajax.send(csvStudenti);
    }

    return {
        dodajStudenta: dodajStudenta,
        postaviGrupu: postaviGrupu,
        dodajBatch: dodajBatch
    }


}())