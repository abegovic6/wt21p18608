window.onload = function () {

    document.getElementById("dugme").addEventListener("click",
        function (event) {
            event.preventDefault();
            let index = document.getElementById("index").value;
            let grupa = document.getElementById("grupa").value;

            StudentAjax.postaviGrupu(index, grupa, (error, data) => {
                if (error != null) {
                    document.getElementById("ajaxstatus").innerHTML = "Greska";
                } else {
                    document.getElementById("ajaxstatus").innerHTML = data.status;
                }
            })
        })

}