

window.onload = function () {
    let dugme = document.getElementById("posalji");
    dugme.className = "hide";
    dugme.addEventListener("click",
        function (event) {
            event.preventDefault();
            console.log("posalji");
            let pogresno = false;

            const brojVjezbi = document.getElementById("brVjezbi").value;

            let s = '{\n\t"brojVjezbi":' + brojVjezbi + ',\n\t"brojZadataka":[';
            for (let i = 0; i < brojVjezbi; i++) {
                let broj = document.getElementById("z" + i).value;
                if (broj < 0 || broj > 10)
                    pogresno = true;
                s += "\n\t\t" + broj + ",";
            }

            if (s[s.length - 1] == ",") s = s.slice(0, -1);

            s += "\n\t]\n}";

            if (pogresno) {
                console.log("Podaci nisu validni");
                return;
            } else {
                console.log("Podaci uspjesno dodani");
                VjezbeAjax.posaljiPodatke(JSON.parse(s), (error, data) => {
                    if (error != null) {
                        console.log("greska");
                    } else if (data != null) {
                        VjezbeAjax.iscrtajVjezbe(element, data);
                    }
                });
            }
        }
    )

    document.getElementById("unesiVjezbe").addEventListener("click", 
    function (event) {
        event.preventDefault();
        console.log("vjezbe");
        const brVjezbi = document.getElementById("brVjezbi").value;
        VjezbeAjax.dodajInputPolja(document.getElementById("zadaci"), brVjezbi);
        if (brVjezbi > 0 && brVjezbi < 16) {
            dugme.className = "show";
        } else {
            dugme.className = "hide";
        }
    });
}