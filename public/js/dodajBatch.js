window.onload = function () {

    document.getElementById("dugme").addEventListener("click",
        function (event) {
            event.preventDefault();
            let csv = document.getElementById("csv").value;

            StudentAjax.dodajBatch(csv, (error, data) => {
                if (error != null) {
                    document.getElementById("ajaxstatus").innerHTML = "Greska";
                } else {
                    document.getElementById("ajaxstatus").innerHTML = data.status;
                }
            })
        })

}