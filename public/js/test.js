let assert = chai.assert;
describe('VjezbeAjax', function () {

    // Testovi za VjezbeAjax.dodajInputPolja()
    describe('dodajInputPolja()', function () {
        it('Test: broj vježbi 0:', function () {
            const element = document.getElementById("testiranje");
            element.innerHTML = '';
            VjezbeAjax.dodajInputPolja(element, 0);
            assert.equal(0, element.children.length, 'Broj insert polja mora biti 0!');
        });
        it('Test: broj vježbi 1:', function () {
            const element = document.getElementById("testiranje");
            element.innerHTML = '';
            VjezbeAjax.dodajInputPolja(element, 1);
            assert.equal(1, element.children.length, 'Broj insert polja mora biti 1!');
        });
        it('Test: broj vježbi 7:', function () {
            const element = document.getElementById("testiranje");
            element.innerHTML = '';
            VjezbeAjax.dodajInputPolja(element, 7);
            assert.equal(7, element.children.length, 'Broj insert polja mora biti 1!');
        });
        it('Test: broj vježbi 15:', function () {
            const element = document.getElementById("testiranje");
            element.innerHTML = '';
            VjezbeAjax.dodajInputPolja(element, 15);
            assert.equal(15, element.children.length, 'Broj insert polja mora biti 15!');
        });
        it('Test: broj vježbi 16:', function () {
            const element = document.getElementById("testiranje");
            element.innerHTML = '';
            VjezbeAjax.dodajInputPolja(element, 16);
            assert.equal(0, element.children.length, 'Broj insert polja mora biti 0!');
        });
    });

    // Testovi za VjezbeAjax.posaljiPodatke()
    chai.should();
    describe('posaljiPodatke()', function () {
        beforeEach(function () {
            this.xhr = sinon.useFakeXMLHttpRequest();

            this.requests = [];
            this.xhr.onCreate = function (xhr) {
                this.requests.push(xhr);
            }.bind(this);
        });

        afterEach(function () {
            this.xhr.restore();
        });

        it('Test: ispravni podaci - 1', function (done) {
            const objekat = { "brojVjezbi": 1, "brojZadataka": [0] };
            VjezbeAjax.posaljiPodatke(objekat, function (err, data) {
                assert.equal(err, null, 'Podaci su uredu!');
                assert.deepEqual(data, objekat);
                done();
            });
            this.requests[0].requestBody.should.equal(JSON.stringify(objekat));
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, JSON.stringify(objekat));
        });
        it('Test: ispravni podaci - 2', function (done) {
            const objekat = { "brojVjezbi": 4, "brojZadataka": [1,2,3,4] };
            VjezbeAjax.posaljiPodatke(objekat, function (err, data) {
                assert.equal(err, null, 'Podaci su uredu!');
                assert.deepEqual(data, objekat);
                done();
            });
            this.requests[0].requestBody.should.equal(JSON.stringify(objekat));
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, JSON.stringify(objekat));
        });
        it('Test: ispravni podaci - 3', function (done) {
            const objekat = { "brojVjezbi": 15, "brojZadataka": [1,2,3,4,5,6,7,8,9,10,1,2,3,4,5] };
            VjezbeAjax.posaljiPodatke(objekat, function (err, data) {
                assert.equal(err, null, 'Podaci su uredu!');
                assert.deepEqual(data, objekat);
                done();
            });
            this.requests[0].requestBody.should.equal(JSON.stringify(objekat));
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, JSON.stringify(objekat));
        });
        it('Test: neispravni podaci - 1', function (done) {
            const objekat = { "brojVjezbi": 17, "brojZadataka": [1,2,3,4] };
            const greska = { status: "error", data: "Pogrešan parametar brojVjezbi,brojZadataka" };
            VjezbeAjax.posaljiPodatke(objekat, function (err, data) {
                assert.equal(err, greska.data, 'Postoji greška u brojuVjezbi i brojuZadataka');
                assert.deepEqual(data, null);
                done();
            });
            this.requests[0].requestBody.should.equal(JSON.stringify(objekat));
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, JSON.stringify(greska));
        });
        it('Test: neispravni podaci -  2', function (done) {
            const objekat = { "brojVjezbi": 5, "brojZadataka": [1,2,3,4] };
            const greska = { status: "error", data: "Pogrešan parametar brojZadataka" };
            VjezbeAjax.posaljiPodatke(objekat, function (err, data) {
                assert.equal(err, greska.data, 'Postoji greška u brojuZadataka');
                assert.deepEqual(data, null);
                done();
            });
            this.requests[0].requestBody.should.equal(JSON.stringify(objekat));
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, JSON.stringify(greska));
        });
        it('Test: neispravni podaci - 3', function (done) {
            const objekat = { "brojVjezbi": 5, "brojZadataka": [1,2,3,4,20] };
            const greska = { status: "error", data: "Pogrešan parametar z4" };
            VjezbeAjax.posaljiPodatke(objekat, function (err, data) {
                assert.equal(err, greska.data, 'Postoji greška u vrijednosti zadatka');
                assert.deepEqual(data, null);
                done();
            });
            this.requests[0].requestBody.should.equal(JSON.stringify(objekat));
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, JSON.stringify(greska));
        });
        it('Test: neispravni podaci - 4', function (done) {
            const objekat = { "brojVjezbi": 17, "brojZadataka": [1,2,3,45] };
            const greska = { status: "error", data: "Pogrešan parametar brojVjezbi,z4,brojZadataka" };
            VjezbeAjax.posaljiPodatke(objekat, function (err, data) {
                assert.equal(err, greska.data, 'Postoji greška u svemu');
                assert.deepEqual(data, null);
                done();
            });
            this.requests[0].requestBody.should.equal(JSON.stringify(objekat));
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, JSON.stringify(greska));
        });
        it('Test: desio se neki error', function (done) {
            const objekat = {};
            VjezbeAjax.posaljiPodatke(objekat, function (err, data) {
                assert.equal(err, "error", 'Error');
                assert.deepEqual(data, null);
                done();
            });
            this.requests[0].respond(404, { 'Content-Type': 'text/json' }, '{"data": "error"}');
        });
    })
    
    // Testovi za VjezbeAjax.dohvatiPodatke()
    describe('dohvatiPodatke()', function () {
        beforeEach(function () {
            this.xhr = sinon.useFakeXMLHttpRequest();

            this.requests = [];
            this.xhr.onCreate = function (xhr) {
                this.requests.push(xhr);
            }.bind(this);
        });

        afterEach(function () {
            this.xhr.restore();
        });
        it('Test: podaci su ispravni - 1', function (done) {
            const objekat = { "brojVjezbi": 1, "brojZadataka": [0] };
            VjezbeAjax.dohvatiPodatke(function (err, data) {
                assert.equal(err, null, 'Podaci su uredu!');
                assert.deepEqual(data, objekat);
                done();
            });
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, JSON.stringify(objekat));
        });
        it('Test: podaci su ispravni - 2', function (done) {
            const objekat = { "brojVjezbi": 4, "brojZadataka": [1,2,3,4] };
            VjezbeAjax.dohvatiPodatke(function (err, data) {
                assert.equal(err, null, 'Podaci su uredu!');
                assert.deepEqual(data, objekat);
                done();
            });
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, JSON.stringify(objekat));
        });
        it('Test: podaci su ispravni - 3', function (done) {
            const objekat = { "brojVjezbi": 15, "brojZadataka": [1,2,3,4,5,6,7,8,9,10,1,2,3,4,5] };
            VjezbeAjax.dohvatiPodatke(function (err, data) {
                assert.equal(err, null, 'Podaci su uredu!');
                assert.deepEqual(data, objekat);
                done();
            });
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, JSON.stringify(objekat));
        });
        it('Test: podaci su neispravni - 1', function (done) {
            VjezbeAjax.dohvatiPodatke(function (err, data) {
                assert.equal(err, "Pogrešan parametar brojZadataka", 'Pogresni podaci!');
                assert.deepEqual(data, null);
                done();
            });
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, '{"brojVjezbi": 1, "brojZadataka":[]}');
        });
        it('Test: podaci su neispravni - 2', function (done) {
            VjezbeAjax.dohvatiPodatke(function (err, data) {
                assert.equal(err, "Pogrešan parametar z0", 'Pogresni podaci!');
                assert.deepEqual(data, null);
                done();
            });
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, '{"brojVjezbi": 1, "brojZadataka":[-1]}');
        });
        it('Test: podaci su neispravni - 3', function (done) {
            VjezbeAjax.dohvatiPodatke(function (err, data) {
                assert.equal(err, "Pogrešan parametar z0,brojZadataka", 'Pogresni podaci!');
                assert.deepEqual(data, null);
                done();
            });
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, '{"brojVjezbi": 1, "brojZadataka":[-1,1]}');
        });
        it('Test: podaci su neispravni - 4', function (done) {
            VjezbeAjax.dohvatiPodatke(function (err, data) {
                assert.equal(err, "Pogrešan parametar brojVjezbi,z0,brojZadataka", 'Pogresni podaci!');
                assert.deepEqual(data, null);
                done();
            });
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, '{"brojVjezbi": -1, "brojZadataka":[-1,1]}');
        });
        it('Test: desio se neki error - 1', function (done) {
            let data1 = { status: 404, data: "error" };
            let dataJson = JSON.stringify(data1);
            VjezbeAjax.dohvatiPodatke(function (err, data) {
                assert.equal(err, "error", 'Desio se neki error!');
                assert.deepEqual(data, null);
                done();
            });
            this.requests[0].respond(404, { 'Content-Type': 'text/json' }, dataJson);
        });
        it('Test: desio se neki error - 1', function (done) {
            VjezbeAjax.dohvatiPodatke(function (err, data) {
                assert.equal(err, "Desio se neki error", 'Desio se neki error!');
                assert.deepEqual(data, null);
                done();
            });
            this.requests[0].respond(404, { 'Content-Type': 'text/json' }, '{"data":"nekiError"}');
        });
    });

    // Testovi za VjezbeAjax.iscrtajVjezbe()
    describe('iscrtajVjezbe()', function () {
        it('Test - broj vježbi pogrešan 0', function () {
            const element = document.getElementById("testiranje");
            element.innerHTML = '';
            VjezbeAjax.iscrtajVjezbe(element, { brojVjezbi: 0, brojZadataka: [] });
            assert.equal(0, element.children.length, 'Podaci su neispravni - funckija ne radi nista!');
        });
        it('Test - broj vježbi 1', function () {
            const element = document.getElementById("testiranje");
            element.innerHTML = '';
            VjezbeAjax.iscrtajVjezbe(element, { brojVjezbi: 1, brojZadataka: [1] });
            assert.equal(1, element.children.length, 'Podaci su ispravni - funckija treba iscrtati 10 vježbi!');
        });
        it('Test - broj vježbi 10', function () {
            const element = document.getElementById("testiranje");
            element.innerHTML = '';
            VjezbeAjax.iscrtajVjezbe(element, { brojVjezbi: 10, brojZadataka: [1,2,3,4,5,6,7,8,9,10] });
            assert.equal(10, element.children.length, 'Podaci su ispravni - funckija treba iscrtati 10 vježbi!');
        });
        it('Test - broj vježbi 15', function () {
            const element = document.getElementById("testiranje");
            element.innerHTML = '';
            VjezbeAjax.iscrtajVjezbe(element, { brojVjezbi: 15, brojZadataka: [1,2,3,4,5,6,7,8,9,10,1,2,3,4,5] });
            assert.equal(15, element.children.length, 'Podaci su ispravni - funckija treba iscrtati 15 vježbi!');
        });
        it('Test - broj vježbi pogrešan 16', function () {
            const element = document.getElementById("testiranje");
            element.innerHTML = '';
            VjezbeAjax.iscrtajVjezbe(element, { brojVjezbi: 16, brojZadataka: [] });
            assert.equal(0, element.children.length, 'Podaci su neispravni - funckija ne radi nista!');
        });
        it('Test - broj zadataka pogrešan 9 vježbi 3 zadatka', function () {
            const element = document.getElementById("testiranje");
            element.innerHTML = '';
            VjezbeAjax.iscrtajVjezbe(element, { brojVjezbi: 9, brojZadataka: [1,2,3] });
            assert.equal(0, element.children.length, 'Podaci su neispravni - funckija ne radi nista!');
        });
        it('Test - broj zadataka pogrešan 9 vježbi 10 zadataka', function () {
            const element = document.getElementById("testiranje");
            element.innerHTML = '';
            VjezbeAjax.iscrtajVjezbe(element, { brojVjezbi: 9, brojZadataka: [1,2,3,4,5,6,7,8,9,10] });
            assert.equal(0, element.children.length, 'Podaci su neispravni - funckija ne radi nista!');
        });
        it('Test - broj zadataka za jednu vježbu pogrešan - negativno', function () {
            const element = document.getElementById("testiranje");
            element.innerHTML = '';
            VjezbeAjax.iscrtajVjezbe(element, { brojVjezbi: 3, brojZadataka: [1,2,-3] });
            assert.equal(0, element.children.length, 'Podaci su neispravni - funckija ne radi nista!');
        });
        it('Test - broj zadataka za jednu vježbu pogrešan > 10', function () {
            const element = document.getElementById("testiranje");
            element.innerHTML = '';
            VjezbeAjax.iscrtajVjezbe(element, { brojVjezbi: 3, brojZadataka: [1,2,12] });
            assert.equal(0, element.children.length, 'Podaci su neispravni - funckija ne radi nista!');
        });
    });

    // Testovi za VjezbeAjax.iscrtajZadatke()
    describe('iscrtajZadatke()', function () {
        it('Test - broj zadataka je negativan', function () {
            const element = document.getElementById("testiranje");
            element.innerHTML = '';
            VjezbeAjax.iscrtajZadatke(element, -1);
            assert.equal(0, element.children.length, 'Podaci su neispravni - funckija ne radi nista!');
        });
        it('Test - broj zadataka je 0', function () {
            const element = document.getElementById("testiranje");
            element.innerHTML = '';
            VjezbeAjax.iscrtajZadatke(element, 0);
            assert.equal(0, element.children.length, 'Podaci su ispravni - funckija treba iscrtati 0 zadataka!');
        });
        it('Test - broj zadataka je 4', function () {
            const element = document.getElementById("testiranje");
            element.innerHTML = '';
            VjezbeAjax.iscrtajZadatke(element, 4);
            assert.equal(4, element.children.length, 'Podaci su ispravni - funckija treba iscrtati 4 zadatka!');
        });
        it('Test - broj zadataka je 10', function () {
            const element = document.getElementById("testiranje");
            element.innerHTML = '';
            VjezbeAjax.iscrtajZadatke(element, 10);
            assert.equal(10, element.children.length, 'Podaci su ispravni - funckija treba iscrtati 10 zadataka!');
        });
        it('Test - broj zadataka je 11', function () {
            const element = document.getElementById("testiranje");
            element.innerHTML = '';
            VjezbeAjax.iscrtajZadatke(element, 11);
            assert.equal(0, element.children.length, 'Podaci su neispravni - funckija ne radi nista!');
        });
    });
});
