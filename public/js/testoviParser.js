let TestoviParser = (function() {
    const isInt = function(n) {
        return Number(n) === n && n % 1 === 0;
    }

    const dajTacnost = function(jsonString) {
        try {
            let obj = JSON.parse(jsonString);
            let brojtestova = obj.stats.tests;
            const brojtacnihtestova = parseFloat(obj['stats']['passes']);
            const brojgresaka = parseFloat(obj['stats']['failures']);

            const procenat = (100 * brojtacnihtestova) / brojtestova;
            let string = "";
            if (isInt(procenat))
                string = "{\"tacnost\":\"" + procenat + "%\",\"greske\":";
            else
                string = "{\"tacnost\":\"" + procenat.toFixed(1) + "%\",\"greske\":";

            if (brojgresaka == 0)
                string = string + "[]}";
            else {
                string = string + "[";
                for (var i = 0; i < brojgresaka; i++) {
                    string = string + "\"" + obj['failures'][i]['fullTitle'] + "\"";
                    if (i != brojgresaka - 1)
                        string = string + ","
                }
                string = string + "]}";
            }
            return JSON.parse(string);
        } catch (e) {
            return JSON.parse("{\"tacnost\":\"0%\",\"greske\":[\"Testovi se ne mogu izvršiti\"]}");
        }
    }

    const provjeriNaziveTestova = function(obj1, obj2) {
        if (obj1.tests.length != obj2.tests.length) {
            return false;
        }
        for (const x1 of obj1.tests) {
            let postojiIme = false;
            for (const x2 of obj2.tests) {
                if (x1.fullTitle == x2.fullTitle)
                    postojiIme = true;
            }
            if (!postojiIme)
                return false;
        }

        return true;
    }

    const izbrojTestove = function(obj1, obj2) {
        let brojac = 0;
        for (const x1 of obj1.failures) {
            let postojiIme = false;
            for (const x2 of obj2.tests) {
                if (x1.fullTitle == x2.fullTitle)
                    postojiIme = true;
            }
            if (!postojiIme) {
                brojac++;
            }

        }
        return brojac;
    }

    const dajJedinstveneGreskeRezultat1 = function(obj1, obj2) {
        let greske = [];
        for (const x1 of obj1.failures) {
            let postojiIme = false;
            for (const x2 of obj2.tests) {
                if (x1.fullTitle == x2.fullTitle)
                    postojiIme = true;
            }
            if (!postojiIme) {
                greske.push(x1);
            }

        }
        return greske;
    }

    const dajJedinstveneGreskeRezultat2 = function(obj1, obj2) {
        let greske = [];
        for (const x1 of obj2.failures) {
            let postojiIme = false;
            for (const x2 of obj1.failures) {
                if (x1.fullTitle == x2.fullTitle)
                    postojiIme = true;
            }
            if (!postojiIme) {
                greske.push(x1);
            }

        }
        return greske;
    }

    const itemsSort = function(a, b) {
        var nameA = a.fullTitle; // ignore upper and lowercase
        var nameB = b.fullTitle; // ignore upper and lowercase
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }

        // names must be equal
        return 0;
    };

    const porediRezultate = function(rezultat1, rezultat2) {
        try {
            var obj1 = JSON.parse(rezultat1);
            var obj2 = JSON.parse(rezultat2);

            const tacnost2 = (dajTacnost(rezultat2).tacnost);

            let promjena = 0;
            let greske = [];
            if (provjeriNaziveTestova(obj1, obj2)) {
                string = "{\"promjena\":\"" + tacnost2 + "\",\"greske\":";
                greske = obj2.failures.sort((a, b) => itemsSort(a, b));

            } else {
                const pomocna = izbrojTestove(obj1, obj2);
                const failures2 = obj2.stats.failures;
                const brojTestova2 = obj2.stats.tests;
                let promjena = (pomocna + failures2) / (pomocna + brojTestova2) * 100;
                if (isInt(promjena))
                    string = "{\"promjena\":\"" + promjena + "%\",\"greske\":";
                else
                    string = "{\"promjena\":\"" + promjena.toFixed(1) + "%\",\"greske\":";
                let greske1 = dajJedinstveneGreskeRezultat1(obj1, obj2).sort((a, b) => itemsSort(a, b));
                let greske2 = obj2.failures.sort((a, b) => itemsSort(a, b));
                greske = greske.concat(greske1);
                greske = greske.concat(greske2);
            }


            if (greske.length == 0)
                string = string + "[]}";
            else {
                string = string + "[";
                for (var i = 0; i < greske.length; i++) {
                    string = string + "\"" + greske[i].fullTitle + "\"";
                    if (i != greske.length - 1)
                        string = string + ","
                }
                string = string + "]}";
            }

            return JSON.parse(string);

        } catch (e) {
            return JSON.parse("{\"promjena\":\"0%\",\"greske\":[\"Testovi se ne mogu izvršiti\"]}");
        }
    }

    return {
        dajTacnost: dajTacnost,
        porediRezultate: porediRezultate
    }
}())



var rezultat1 = `{
    "stats":{
       "suites":3,
       "tests":3,
       "passes":2,
       "pending":0,
       "failures":1,
       "start": "2021-11-05T15:00:26.343Z",
       "end": "2021-11-05T15:00:26.352Z",
       "duration": 20

    },
    "tests":[
       {
          "title":"test1",
          "fullTitle":"test1",
          "file":null,
          "duration":1,
          "currentRetry":0,
          "speed":"fast",
          "err":{
             
          }
       },
       {
          "title":"test2",
          "fullTitle":"test2",
          "file":null,
          "duration":0,
          "currentRetry":0,
          "speed":"fast",
          "err":{
             
          }
       },
       {
          "title":"test5",
          "fullTitle":"test5",
          "file":null,
          "duration":0,
          "currentRetry":0,
          "speed":"fast",
          "err":{
             
          }
       }
    ],
    "pending":[],
    "failures":[
      {
         "title":"test5",
         "fullTitle":"test5",
         "file":null,
         "duration":1,
         "currentRetry":0,
         "speed":"fast",
         "err":{}
      }
  ],
    "passes":[
       {
          "title":"test1",
          "fullTitle":"test1",
          "file":null,
          "duration":1,
          "currentRetry":0,
          "speed":"fast",
          "err":{}
       },
       {
          "title":"test2",
          "fullTitle":"test2",
          "file":null,
          "duration":1,
          "currentRetry":0,
          "speed":"fast",
          "err":{}
       }
    ]
 }`
var rezultat2 = `{
  "stats":{
     "suites":3,
     "tests":3,
     "passes":1,
     "pending":0,
     "failures":2,
     "start": "2021-11-05T15:00:26.343Z",
     "end": "2021-11-05T15:00:26.352Z",
     "duration": 20

  },
  "tests":[
     {
        "title":"test6",
        "fullTitle":"test6",
        "file":null,
        "duration":1,
        "currentRetry":0,
        "speed":"fast",
        "err":{
           
        }
     },
     {
        "title":"test4",
        "fullTitle":"test4",
        "file":null,
        "duration":0,
        "currentRetry":0,
        "speed":"fast",
        "err":{
           
        }
     },
     {
        "title":"test3",
        "fullTitle":"test3",
        "file":null,
        "duration":0,
        "currentRetry":0,
        "speed":"fast",
        "err":{
           
        }
     }
  ],
  "pending":[],
  "failures":[
    {
       "title":"test4",
       "fullTitle":"test4",
       "file":null,
       "duration":1,
       "currentRetry":0,
       "speed":"fast",
       "err":{}
    },
     {
      "title":"test3",
      "fullTitle":"test3",
      "file":null,
      "duration":1,
      "currentRetry":0,
      "speed":"fast",
      "err":{}
   }
  ],
  "passes":[
     {
        "title":"test6",
        "fullTitle":"test6",
        "file":null,
        "duration":1,
        "currentRetry":0,
        "speed":"fast",
        "err":{}
     }
  ]
}`

TestoviParser.porediRezultate(rezultat1, rezultat2);