window.onload = function () {

    document.getElementById("dugme").addEventListener("click",
        function (event) {
            event.preventDefault();
            let ime = document.getElementById("ime");
            let prezime = document.getElementById("prezime");
            let index = document.getElementById("index");
            let grupa = document.getElementById("grupa");
            const student = {
                ime: ime.value,
                prezime: prezime.value,
                index: index.value,
                grupa: grupa.value
            }

            StudentAjax.dodajStudenta(student, (error, data) => {
                if (error != null) {
                    document.getElementById("ajaxstatus").innerHTML = "Greska";
                } else {
                    document.getElementById("ajaxstatus").innerHTML = data.status;
                }
            })
        })

}
