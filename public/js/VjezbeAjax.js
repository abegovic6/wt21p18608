var VjezbeAjax = (function () {
    const dodajInputPolja = function (DOMelementDIVauFormi, brojVjezbi) {
        while (DOMelementDIVauFormi.firstChild) {
            DOMelementDIVauFormi.removeChild(DOMelementDIVauFormi.firstChild);
        }

        if (brojVjezbi < 1 || brojVjezbi > 15) {
            console.log("Podaci nisu validni");
            return;
        }

        for (let i = 0; i < brojVjezbi; i++) {
            var newLabel = '<label>z' + i + '</label>'
            var input = document.createElement("input");
            input.type = "number";
            input.name = "z" + i;
            input.id = "z" + i;
            input.value = 4;
            input.min = 0;
            input.max = 10;
            var parser = new DOMParser();
            var newNode = parser.parseFromString(newLabel, "text/xml");
            var childDom = document.createElement("div");
            childDom.appendChild(newNode.documentElement);
            childDom.appendChild(input);
            childDom.appendChild(document.createElement("br"));
            DOMelementDIVauFormi.appendChild(childDom);
        }
    }

    const posaljiPodatke = function (vjezbeObjekat, callbackFja) {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function () {
            if (ajax.readyState == 4 && ajax.responseText.includes("error"))
                callbackFja(JSON.parse(ajax.responseText)['data'], null);
            else if (ajax.readyState == 4 && ajax.status == 200) {
                callbackFja(null, JSON.parse(ajax.responseText));
            }
            else if (ajax.readyState == 4 && ajax.status == 404)
                callbackFja("error", null);
        }

        ajax.open("POST", "http://localhost:3000/vjezbe", true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify(vjezbeObjekat));
    }


    const dohvatiPodatke = function (callbackFja) {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function () {
            if (ajax.readyState == 4 && ajax.responseText.includes("error"))
                callbackFja(JSON.parse(ajax.responseText)['data'], null);
            else if (ajax.readyState == 4 && ajax.status == 200) {
                const body = JSON.parse(ajax.responseText);
                let brVjezbi = null;
                let nizZadataka = null;
                let brZadataka = null;

                if (body['brojVjezbi'] != null && body['brojVjezbi'] > 0 && body['brojVjezbi'] < 16)
                    brVjezbi = null
                else brVjezbi = "brojVjezbi";

                if (body['brojVjezbi'] == null || body['brojVjezbi'] != body['brojZadataka'].length){
                    console.log("uslo");
                    brZadataka = "brojZadataka";
                }
                    

                if (body['brojVjezbi'] == null) return null;
                let s = "";
                for (let i = 0; i < body['brojZadataka'].length; i++) {
                    if (body['brojZadataka'][i] < 0 || body['brojZadataka'][i] > 10) {
                        s += "z" + i + ",";
                    }
                }
                if (s != "") {
                    s = s.slice(0, -1);
                    nizZadataka = s;
                }

                let netacanString = "{\"status\":\"error\",\"data\":\"Pogrešan parametar ";
                let netacan = false;

                if (brVjezbi != null) {
                    netacanString += brVjezbi;
                    if (brZadataka != null || nizZadataka != null)
                        netacanString += ",";
                    netacan = true;
                }
                if (nizZadataka != null) {
                    netacanString += nizZadataka;
                    if (brZadataka != null)
                        netacanString += ",";
                    netacan = true;
                }
                if (brZadataka != null) {
                    netacanString += brZadataka;
                    netacan = true;
                }

                if(netacan) {
                    netacanString += "\"}";
                    callbackFja(JSON.parse(netacanString)['data'], null);
                } else {
                    callbackFja(null, JSON.parse(ajax.responseText));
                }
                
            }
            else if (ajax.readyState == 4) {
                callbackFja("Desio se neki error", null);
            }
        }


        ajax.open("GET", "http://localhost:3000/vjezbe/", true);
        ajax.send();
    }

    var proslaVjezba;
    const iscrtajVjezbe = (divDOMelement, objekat) => {
        const brojVjezbi = objekat['brojVjezbi'];
        let nacrtaj = true;
        if (brojVjezbi < 1 || brojVjezbi > 16) {
            nacrtaj = false;
        }
        const niz = objekat['brojZadataka'];
        if (brojVjezbi !== niz.length) {
            nacrtaj = false;
        }
        for (let i = 0; i < brojVjezbi; i++) {
            if (niz[i] < 0 || niz[i] > 10) {
                nacrtaj = false;
            }
        }

        if (nacrtaj === false) {
            console.log("Pogresni podaci");
            return;
        }

        for (let i = 1; i < brojVjezbi + 1; i++) {
            let vjezba = document.createElement("li");
            let button = document.createElement("button");
            button.innerHTML = "Vjezba " + i;
            button.className = "vjezba";
            vjezba.name = "prviput";
            button.addEventListener("click", function (event) {
                event.preventDefault();
                let element = document.getElementById("v" + i);
                if (proslaVjezba == undefined) {
                    proslaVjezba = element;
                } else {
                    proslaVjezba.className = "hide-horizontal";
                    proslaVjezba = element;
                }
                if (vjezba.name == "prviput") {
                    iscrtajZadatke(element, objekat['brojZadataka'][i - 1]);
                    vjezba.name = "vidljivo";
                } else if (vjezba.name == "vidljivo") {
                    element.className = "hide-horizontal";
                    vjezba.name = "nevidljivo";
                } else if (vjezba.name == "nevidljivo") {
                    element.className = "show-horizontal";
                    vjezba.name = "vidljivo";
                }

            });
            vjezba.appendChild(button);

            var childDom = document.createElement("div");
            childDom.appendChild(vjezba);

            let li = document.createElement("li");
            let ul = document.createElement("ul");
            ul.className = "show-horizontal";
            ul.id = "v" + i;

            li.appendChild(ul);
            childDom.appendChild(li);
            childDom.appendChild(document.createElement("br"));
            divDOMelement.appendChild(childDom);
        }
    }


    const iscrtajZadatke = (vjezbaDOMelement, brojZadataka) => {
        if (brojZadataka < 0 || brojZadataka > 10) {
            console.log("Greška u podacima");
            return;
        }
        for (let i = 0; i < brojZadataka; i++) {
            let zadatak = document.createElement("li");
            let button = document.createElement("button");
            button.innerHTML = "Zadatak " + (i+1);
            button.className = "zadatak";

            zadatak.appendChild(button);
            vjezbaDOMelement.appendChild(zadatak);
        }
    }

    return {
        posaljiPodatke: posaljiPodatke,
        dodajInputPolja: dodajInputPolja,
        dohvatiPodatke: dohvatiPodatke,
        iscrtajVjezbe: iscrtajVjezbe,
        iscrtajZadatke: iscrtajZadatke
    }
}())