var DataTypes = require('sequelize/lib/data-types');

const Sequelize = require('sequelize');
const bodyParser = require('body-parser');
const sequelize = require('./baza.js');
const express = require("express");
const { urlencoded } = require('express');
const student = require('./modeli/student.js');
const app = express();

const Grupa = require('./modeli/grupa.js')(sequelize);
const Student = require('./modeli/student.js')(sequelize);

const Vjezba = require('./modeli/vjezba.js')(sequelize);
const Zadatak = require('./modeli/zadatak.js')(sequelize);

app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/public/html'));
app.use(express.static(__dirname + '/public/css'));
app.use(express.static(__dirname + '/public/json'));
app.use(express.static(__dirname + '/public/images'));
app.use(bodyParser.json())
app.use(bodyParser.raw());
app.use(bodyParser.text());




Grupa.hasMany(Student, {
  foreignKey: {
    name: 'grupa',
    type: DataTypes.STRING
  }
});

Vjezba.hasMany(Zadatak);



sequelize.sync().then(res => {
  console.log(res)
}).catch(err => console.log(err));

app.get('/vjezbe', function (req, res) {
  let brVjezbi = null;
  Vjezba.findAll({
    order: [['index']]
  }).then(function (svevjezbeizbaze) {
    console.log(svevjezbeizbaze)
    brVjezbi = svevjezbeizbaze.length;

    Promise.all(svevjezbeizbaze.map(vjezba => {
      return Zadatak.findAll({
        where: {
          VjezbaId: vjezba.id
        }
      }).then(function (zadaci) {
        return zadaci.length;
      });
    })).then(function (results) {
      const objekat = { "brojVjezbi": brVjezbi, "brojZadataka": results };
      res.json(objekat);
    })
  })
});


const provjeriBrojVjezbi = (brVjezbi) => {
  if (brVjezbi != null && brVjezbi > 0 && brVjezbi < 16)
    return null;
  else return "brojVjezbi";
}

const provjeriBrojZadataka = (brVjezbi, velicinaNiza) => {
  if (brVjezbi == null || brVjezbi != velicinaNiza)
    return "brojZadataka";
  return null;
}

const provjeriNizZadataka = (brVjezbi, nizZadataka) => {
  if (brVjezbi == null) return null;
  let s = "";
  for (let i = 0; i < nizZadataka.length; i++) {
    if (nizZadataka[i] < 0 || nizZadataka[i] > 10) {
      s += "z" + i + ",";
    }
  }
  if (s != "") {
    s = s.slice(0, -1);
    return s;
  }
  return null;
}

app.post('/vjezbe', function (req, res) {
  let netacanString = "{\"status\":\"error\",\"data\":\"Pogrešan parametar ";
  let netacan = false;

  const brVjezbi = provjeriBrojVjezbi(req.body['brojVjezbi']);
  const nizZadataka = provjeriNizZadataka(req.body['brojVjezbi'], req.body['brojZadataka']);
  const brZadataka = provjeriBrojZadataka(req.body['brojVjezbi'], req.body['brojZadataka'].length);

  if (brVjezbi != null) {
    netacanString += brVjezbi;
    if (brZadataka != null || nizZadataka != null)
      netacanString += ",";
    netacan = true;
  }
  if (nizZadataka != null) {
    netacanString += nizZadataka;
    if (brZadataka != null)
      netacanString += ",";
    netacan = true;
  }
  if (brZadataka != null) {
    netacanString += brZadataka;
    netacan = true;
  }

  if (netacan) {
    netacanString += "\"}";
    res.json(JSON.parse(netacanString));
  } else {

    Zadatak.destroy({
      where: {},
      truncate: { cascade: true }
    }).then(function () {
      Vjezba.destroy({
        where: {},
        truncate: { cascade: true }
      }).then(function () {
        let niz = [];
        for (let i = 0; i < req.body['brojVjezbi']; i++) {
          niz.push(i);
        }

        Promise.all(niz.map(broj => {
          return Vjezba.create({
            index: broj,
            naziv: "Vjezba " + broj
          }).then(function (vjezba) {
            return vjezba;
          });
        })).then(function (results) {
          console.log("Vjezba kreirana")
          Promise.all(results.map(vjezba => {
            let niznovi = [];
            for (let i = 0; i < req.body['brojZadataka'][vjezba.index]; i++) {
              niznovi.push(i);
            }
            return Promise.all(niznovi.map(zadatak => {
              return Zadatak.create({
                naziv: "Zadatak " + zadatak,
                VjezbaId: vjezba.id
              })
            })).then(function (results) {
              console.log("Zadatak kreiran");
            })
          })).then(function (results) {
            console.log("Svi zadaci za vjezbu kreirani");
            res.json(req.body);
          })
        })
      })
    });
  }
});

const provjera = (x) => {
  return x == null || x == undefined;
}

app.post('/student', function (req, res) {
  const ime = req.body['ime'];
  const prezime = req.body['prezime'];
  const index = req.body['index'];
  const grupa = req.body['grupa'];

  if (provjera(ime) || provjera(prezime) || provjera(index) || provjera(grupa)) {
    res.json({
      status: "Neki podatak je prazan!"
    });
  } else {
    Student.findAll({
      where: {
        index: index
      }
    }).then(function (svistudentisaindexom) {
      if (svistudentisaindexom.length > 0) {
        res.json({
          status: "Student sa indexom " + index + " već postoji!"
        });
      } else {
        Grupa.findAll({
          where: {
            naziv: grupa
          }
        }).then(function (grupe) {
          if (grupe.length == 0) {
            Grupa.create({
              naziv: grupa
            }).then(function (kreiranagrupa) {
              Student.create({
                ime: ime,
                prezime: prezime,
                index: index,
                grupa: kreiranagrupa.naziv
              }).then(function () {
                res.json({
                  status: "Kreiran student!"
                });
              })
            })
          } else {
            Student.create({
              ime: ime,
              prezime: prezime,
              index: index,
              grupa: grupe[0].naziv
            }).then(function () {
              res.json({
                status: "Kreiran student!"
              });
            })
          }
        })
      }
    })
  }
});

app.put('/student/:index', (req, res) => {
  const index = req.params.index;
  const grupa = req.body['grupa'];

  if (provjera(grupa) || provjera(index)) {
    res.json({
      status: "Neki podatak je prazan!"
    });
  } else {
    Student.findOne({
      where: {
        index: index
      }
    }).then(function (student) {
      if (student == null) {
        res.json({
          status: "Student sa indexom " + index + " ne postoji"
        });
      } else {
        Grupa.findOne({
          where: {
            naziv: grupa
          }
        }).then(function (pretrazenaGrupa) {
          if (pretrazenaGrupa == null) {
            Grupa.create({
              naziv: grupa
            }).then(function (kreiranagrupa) {
              student.update({
                grupa: kreiranagrupa.naziv
              }).then(function () {
                res.json({
                  status: "Promjenjena grupa studentu " + index
                });
              })
            });
          } else {
            student.update({
              grupa: pretrazenaGrupa.naziv
            }).then(function () {
              res.json({
                status: "Promjenjena grupa studentu " + index
              });
            })
          }
        })
      }
    })
  }
});

const grupaPostoji = (grupe, nazivGrupe) => {
  for (let i = 0; i < grupe.length; i++) {
    if (grupe[i].toLowerCase() === nazivGrupe.toLowerCase()) {
      return true;
    }
  }   

  return false;
}

app.post('/batch/student', (req, res) => {
  const str = req.body;

  var niz_stringova = str.toString('utf-8').split(/[\r\n,]/).filter(s => s.length > 0);


  if (niz_stringova.length % 4 != 0) {
    console.log('Nešto nije uredu sa CSV datotekom. Neki podatak fali.');
    res.json( {
      status: 'Nešto nije uredu sa CSV datotekom. Neki podatak fali.' 
    });
    return;
  } else {
    const studentiIzCsv = [];
    for (let i = 0; i < niz_stringova.length; i += 4) {
      studentiIzCsv.push({
        ime: niz_stringova[i],
        prezime: niz_stringova[i + 1],
        index: niz_stringova[i + 2],
        grupa: niz_stringova[i + 3]
      })
    }

    Grupa.findAll().then(function (doSadKreiraneGrupe) {
      Student.findAll().then(function (doSadKreiraniStudenti) {
        const studentiKojiSuKreirani = [];
        const studentiKojiNisuKreirani = [];
        const potrebneGrupeZaKreiranje = [];
        const doSadKreiraneGrupeStringovi = doSadKreiraneGrupe.map(g => g.naziv);


        for (let i = 0; i < studentiIzCsv.length; i++) {
          let kreiran = false;
          for (let j = 0; j < doSadKreiraniStudenti.length; j++) {
            if (doSadKreiraniStudenti[j].index == studentiIzCsv[i].index) {
              kreiran = true;
            }
          }
          if (!kreiran) {
            let prvi = i;
            for (let j = 0; j < studentiIzCsv.length; j++) {
              if (i !== j && studentiIzCsv[i].index === studentiIzCsv[j].index) {
                if (j < i) prvi = j;
                kreiran = true;
              }
            }
            if (kreiran && prvi === i) {
              kreiran = false;
            }
          }
          if (kreiran) {
            studentiKojiSuKreirani.push(studentiIzCsv[i]);
          } else {
            studentiKojiNisuKreirani.push(studentiIzCsv[i]);
            if (!grupaPostoji(doSadKreiraneGrupeStringovi, studentiIzCsv[i].grupa)) {
            if (!grupaPostoji(potrebneGrupeZaKreiranje, studentiIzCsv[i].grupa)) {
              potrebneGrupeZaKreiranje.push(studentiIzCsv[i].grupa);
            }
          }
          }

          
        }



        Promise.all(potrebneGrupeZaKreiranje.map(grupa => {
          return Grupa.create({
            naziv: grupa
          })
        })).then(function () {
          Promise.all(studentiKojiNisuKreirani.map(student => {
            return Student.create({
              ime: student.ime,
              prezime: student.prezime,
              index: student.index,
              grupa: student.grupa
            }).then(function (kreiranistudent) {
              return kreiranistudent;
            })
          })).then(function () {
            let status = "";
            if (studentiKojiSuKreirani.length === 0) {
              status = "Dodano " + studentiKojiNisuKreirani.length + " studenata!";
            } else {
              const indexi = studentiKojiSuKreirani.map(a => a.index).join(',');
              status = "Dodano " + studentiKojiNisuKreirani.length + " studenata, a studenti " + indexi + " već postoje!";
            }
            res.json({
              status: status
            })
          })
        })
      })
    })
  }
});


var server = app.listen(3000);
module.exports = server
